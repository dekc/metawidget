# React/Metawidget exploration 

## Background
The investigation involves a simple SPA that has 2 pages: Home and MetaWidget. The Home page simply describes what the appplication is attempting to do. Navigating to the [MetaWidget](https://github.com/metawidget/metawidget) page will initiate a REST call (to an online fake REST API - [JSONPlaceholder](https://jsonplaceholder.typicode.com/) ) to retrieve JSON user data.
The data is then used to populate a simple master/detail like page. Upon selecting a particular widget a further (and essentially uneccssary ) request is made to retrieve data for the selected widget. 

This JSON data is then passed verbatim into a React component (`src/components/Widget/Widget.js`) that is simply behaving as a wrapper containing the metawidget. The metawidget itself, is responsible for the displaying and data handling. There are however hooks into the metawidget object that allows for the customization of layout formating and data handling. These have yet to be taken advantage of, in order to bridge metawidget's data event handling with React's state handling flow.


`create-react-app` was used to bootstrap the project.

## Directions
After download or cloning the project, move in the directory and run the following commands:
1. `npm install` or `yarn install`
2. `npm start` or `yarn start`
