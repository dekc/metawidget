import React, { Component } from 'react';
import { Switch, Route, NavLink } from 'react-router-dom';
import Home from './pages/Home';
import Users from './pages/Users';
import MetaWidgetTest from './pages/MetaWidgetTest';
import logo from './AvanTek-logo.png';
import './App.css';

class App extends Component {
  render() {
    let routes = (
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/home" component={Home}/>
        <Route path="/users" component={Users}/>
        <Route path="/mw" component={MetaWidgetTest}/>
      </Switch>
    )

    return (
      <div className="App">
        <header >
          <nav>       
            <ul className="NavBar">
            <img src={logo} className="App-logo" alt="logo" />
              <li className="NavItem">
                <NavLink to="/home" activeClassName="Selected">Home</NavLink>
              </li>
              <li className="NavItem">
                <NavLink to="/mw" activeClassName="Selected">MetaWidget</NavLink>
              </li>
              {/*
              <li className="NavItem">
                <NavLink to="/users" activeClassName="Selected">Users</NavLink>
              </li>
              */}
            </ul>
          </nav>          
        </header>
        {routes}
      </div>
    );
  }
}

export default App;
