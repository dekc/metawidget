import React from 'react';
import './User.css';
const User = ({data}) => (
  <article className="User">
    <h5>{data.name}</h5>
    <h6>{data.email}</h6>
  </article>
);

export default User;
