import React, { Component } from 'react';
import metawidget from 'metawidget';
import util from 'util';

import './Widget.css';

class Widget extends Component {
  constructor(props) {
    super(props);
    this.mw = null;
    this.mwElement = null;
    this.state = {
      widgetData: null,
    }
  }

  async componentDidUpdate() {
    if (this.props.id) {
      if (!this.state.widgetData || (this.state.widgetData && this.state.widgetData.id !== this.props.id)) {
        console.log(`Retrieving data for id: ${this.props.id}`);
        const results = await fetch(`${this.props.url}/${this.props.id}`);
        const json = await results.json();
        this.setState({
          widgetData: json,
        });

        // Ensure any exising metawidget data is cleared as this component is 
        // reused when a further selection is performed in the widget-list 
        // component. 
        this.mw && this.mw.clearWidgets();

        // TODO: need to override config in order to:
        // 1. better styling and component layout
        // 2. interface with event handling to enable state update.
        // Look at config (interesting) elements: 
        //    inspector
        //    [prepend | append]widgetProcessors
        //
        // Look at overriding SimpleBindingProcessor
        const config = {
          layout: new metawidget.layout.HeadingTagLayoutDecorator(
            new metawidget.layout.TableLayout( { numberOfColumns: 1 } ))
        };

        console.log(util.inspect(this.state.widgetData));

        this.mw = new metawidget.Metawidget(this.mwElement, config);
        this.mw.toInspect = {...this.state.widgetData};
        this.mw.buildWidgets();
      }
    }
  }

  onClick = () => {
    this.mw.save();
    console.log(util.inspect(this.mw.toInspect));
  }

  render() {
    let widget = <h6>Select a Widget...</h6>;
    if (this.props.id) {
      widget = <h6>Loading...</h6>;
    }
    if (this.state.widgetData) {
      widget = (
        <div className="Widget">
          <div ref={ el => this.mwElement = el }>
          </div>
          <button className="btn far fa-save WidgetSave" onClick={this.onClick}> Save</button>
        </div>
      );
    }
    return widget;
  }
}

export default Widget;
