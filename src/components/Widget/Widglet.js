import React from 'react';

const Widglet = (props) => (
  <div className="Widglet" onClick={props.clicked}>
    <h4>{props.name}</h4>
    <h5>{props.email}</h5>
  </div>
);

export default Widglet;
