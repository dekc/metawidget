import React, { Component } from 'react';
import User from '../../components/User/User';
import './UserList.css';

class UserList extends Component {

  state = {
    users: [],
    loading: false
  };

  async componentDidMount() {
    this.setState({
      ...this.state,
      loading: true
    });
    console.log('fetching ' + this.props.url);
    const results = await fetch(this.props.url);
    const json = await results.json();
    console.log(`Got ${json.length} users`);
    this.setState( {
      users: json,
      loading: false
    });
  }

  render() {
    const users = this.state.users.map( user => {
      return <User key={user.id} data={user} />;
    });

    return (
      <div>
        <section className="Users">
          { this.state.loading ? (<p>Loading...</p>) : users } 
        </section>
      </div>
    );
  }
}

export default UserList;