import React, { Component } from 'react';
import util from 'util';
import Widget from '../../components/Widget/Widget';
import Widglet from '../../components/Widget/Widglet';
import './WidgetList.css';

class WidgetList extends Component {

  constructor(props) {
    super(props);
    this.navLinks = {};
  }

  state = {
    widgets: [],
    loading: false,
    selectedId: null,
    navCmd: null
  };

  componentDidMount() {
    const url = `${this.props.url}?_page=1&_limit=${this.props.limit}`;
    this.fetchData(url);
  }

  async fetchData(url) {
    this.setState({
      ...this.state,
      loading: true
    });
    let data = [];
    try {
      console.log('fetching ' + url);
      const response = await fetch(url);
      const json = await response.json();
      this.setNavCallbacks(response.headers.get('link').split(','));
      if (Array.isArray(json)) {
        data = json;
      }
      else {
        data.push(json);
      }
    }
    catch (err) {
      console.log(err);
    }
    console.log(`Got ${data.length} widgets`);
    this.setState( {
      widgets: data,
      loading: false
    });
  }

  componentDidUpdate() {
    if (this.state.navCmd) {
      console.log('componentDidUpate');
      this.fetchData(this.navLinks[this.state.navCmd]);
      this.setState({
        ...this.state,
        navCmd: null
      });
    }
  }

  setNavCallbacks(links) {
    this.navLinks = {}; // clean out old navigation urls
    links.map( (link) => {
      const ln = link.split(';');
      const url = /<(.*?)>/.exec(ln[0].trim());
      const nav = /first|next|last|prev/.exec(ln[1]);
      if (nav) this.navLinks[nav] = url[1];
    });
  }

  onWidgetSelected = (id) => {
    console.log(`Selected widget id: ${id}`);
    this.setState({
      ...this.state,
      selectedId: id
    });
  }

  onClickedNavButton = (id) => {
    console.log(`Navigating to ${id}`);
    this.setState({
      ...this.state,
      navCmd: id
    });
  }

  render() {
    const widgets = this.state.widgets.map( widget => {
      return <Widglet 
        key={widget.id} 
        name={widget.name}
        email={widget.email} 
        clicked = {() => this.onWidgetSelected(widget.id)} />;
    });

    // const navListButtons = null;
    const navListButtons = (
      <nav className="NavListButtons">
        {(this.navLinks.first) ? <button className="btn NavButton" onClick={() => this.onClickedNavButton('first')}><i className="fas fa-fast-backward"></i></button> : null }
        {(this.navLinks.prev) ?  <button className="btn NavButton" onClick={() => this.onClickedNavButton('prev')}><i className="fas fa-step-backward"></i></button> : null }
        {(this.navLinks.next) ?  <button className="btn NavButton" onClick={() => this.onClickedNavButton('next')}><i className="fas fa-step-forward"></i></button> : null }
        {(this.navLinks.last) ?  <button className="btn NavButton" onClick={() => this.onClickedNavButton('last')}><i className="fas fa-fast-forward"></i></button> : null }
      </nav>
    );

    return (
      <div>
        <section className="Widgets">
          { this.state.loading ? (<p>Loading...</p>) : widgets } 
        </section>
        { this.state.loading ? null : navListButtons } 
        <hr/>
        <section className="Details">
          <Widget id={this.state.selectedId} url={this.props.url} />
        </section>
      </div>
    );
  }
}

export default WidgetList;
