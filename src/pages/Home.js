import React from 'react';

const Home = () => (
  <div>
    <h1 className="App-title">Home Page</h1>
    <section className="App-content">
      <p>The investigation involves a simple SPA that has 2 pages: 
      Home and MetaWidget. The Home page simply describes what the 
      appplication is attempting to do. 
      Navigating to the <a href="https://github.com/metawidget/metawidget">MetaWidget </a>
      page will initiate a REST call (to an online fake REST API - 
      <a href="https://jsonplaceholder.typicode.com/">JSONPlaceholder</a> ) 
      to retrieve JSON user data. The data is then used to populate a simple 
      master/detail like page. Upon selecting a particular widget a further 
      (and essentially uneccssary ) request is made to retrieve data for the 
      selected widget. 
      </p>
      <p>
      This JSON data is then passed verbatim into a React component that is 
      simply behaving as a wrapper containing the metawidget. The metawidget 
      itself, is responsible for the displaying and data handling. There are 
      however hooks into the metawidget object that allows for the 
      customization of layout formating and data handling. 
      </p>
    </section>
  </div>
);

export default Home;
