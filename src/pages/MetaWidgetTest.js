import React, { Component } from 'react';
import WidgetList from '../containers/widgetList/WidgetList';

class MetaWidgetTest extends Component {

  render () {
    return (
      <div>
        <h1 className="App-title">MetaWidget Example</h1>
        <WidgetList url="http://jsonplaceholder.typicode.com/users" limit={4} />
      </div>
    );
  }

}
export default MetaWidgetTest;
