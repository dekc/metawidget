import React, { Component } from 'react';
import UserList from '../containers/UserList/UserList';

class Users extends Component {

  render () {
    return (
      <div>
        <h1 className="App-title">Standard React List</h1>
        <UserList url="https://jsonplaceholder.typicode.com/users"/>
      </div>
    );
  }

}
export default Users;